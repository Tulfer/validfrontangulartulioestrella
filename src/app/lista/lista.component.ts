import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})


export class ListaComponent implements OnInit {

  private urlapi = 'http://localhost:8080/personas';
  private headers = new HttpHeaders().set('Content-Type','application/json');
  personas: any = [];
  form: FormGroup;

  constructor(private http: HttpClient, private fb: FormBuilder) {
    this.form = this.fb.group({
      personasChecked: this.fb.array([])
    })
  }
  

  ngOnInit(): void {
    this.mostrarPersonas();
  }

  mostrarPersonas() {
         
    this.http.get(`${this.urlapi}/consultar`,
      {
        headers: this.headers,
        responseType: "json"
      })
      .subscribe((res: {}) => {
        this.personas = res;
        console.log(this.personas);
      })
      
  }
  
  onChangePersonas(e) {
    const personasChecked: FormArray = this.form.get('personasChecked') as FormArray;
  
    if (e.target.checked) {
      personasChecked.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      personasChecked.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          personasChecked.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  procesarPersonas() {
    console.log(JSON.stringify(this.form.value));
    let idPersonas: any = [];
    let seleccionados = this.form.value.personasChecked;
    for (let i = 0; i < seleccionados.length; i++){
      let persona = {
        "id":seleccionados[i]
      }
      idPersonas.push(persona);
    }

    let jsonIdPersonas = JSON.stringify(idPersonas);
    this.http.put(`${this.urlapi}/procesar`,
    jsonIdPersonas, {headers: this.headers})
    .subscribe(
        (val) => {
          console.log("Ok", val);
          this.mostrarPersonas();
        },
      response => {
        console.log("Error", response);
    });
  }

}
