import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  nombre: String;
  apellido: String;

  registro: boolean = false;
  tipoResultado: String;
  mensajeOk: String = "Registro Exitoso";
  mensajeError: String = "Ha ocurrido un error";
  mensajeWarning: String = "Debe llenar los campos";
  mensaje: String;

  private urlapi = 'http://localhost:8080/personas';
  
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
  }

  guardarRegistro() {
    console.log("Ingreso a guardar, valores: ", this.nombre, this.apellido);
    if (this.nombre != "" && this.nombre != null && this.apellido != "" && this.apellido != null) {
      
      let persona = {
        "nombre": this.nombre,
        "apellido" : this.apellido
      }
      let jsonPersona = JSON.stringify(persona);
      let headers = new HttpHeaders().set('Content-Type','application/json');
      this.http.post(`${this.urlapi}/guardar`,
      jsonPersona, {headers: headers})
      .subscribe(
          (val) => {
            console.log("Ok", val);
            this.tipoResultado = "success";
            this.mensaje = this.mensajeOk;
            this.registro = true;
            setInterval(() => this.registro = false, 1500);
          },
        response => {
          this.tipoResultado = "danger";
          this.mensaje = this.mensajeError;
          this.registro = true;
          setInterval(() => this.registro = false, 1500);
          console.log("Error", response);
      });
    } else {
      this.tipoResultado = "warning";
      this.mensaje = this.mensajeWarning;
      this.registro = true;
      setInterval(() => this.registro = false, 1500);
    }
  }

}
